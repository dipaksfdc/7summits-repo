@IsTest
    global class summitsAccTriggerHandlerTest {	//Class name's first letter should be Capital Letter SummitsAccTriggerHandlerTest

    @TestSetup
    static void testSetup(){
        Integer count = 100;
        List<Account> accList = new List<Account>();
        for(Integer i = 0 ; i < 100 ; i++){
            Account temp = new Account(Name = 'Test Acc ' + i);
            accList.add(temp);
        }
        insert accList;

        List<Contact> contactList = new List<Contact>();
        for(Integer i = 0 ; i < accList.size() ; i++){
            Contact temp = new Contact(LastName = 'Test Contact ' + i, AccountId = accList[i].Id);
            contactList.add(temp);
        }
        insert contactList;
    }

    @IsTest
    static void testWebsite(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId FROM Account];
        for(Account a : accList){
            a.RecordTypeId = summitsAccTriggerHandler.partnerRecordType;// Retrieve RecordTypeID by using Schema method
    //Schema.SobjectType.ObjectName.getRecordTypeInfosByName().get(Label.RecordTypeName).getRecordTypeId();
        }
        try {
            update accList;
        }catch (Exception e){
            System.debug(System.LoggingLevel.FINER, 'Exception: ' + e);
            //Assert will pass
            System.assert(e.getMessage().contains('This Account is a partner account and thus needs a website!'));
        }
    }
    @IsTest
    static void testMatchingAddresses(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId, BillingCountry FROM Account];
        List<Contact> contactList = [SELECT Id, MailingCountry FROM Contact];

        //Assert will pass
        for(Contact a : contactList){
            System.assert(a.MailingCountry == null);
        }
        for(Account a : accList){
            a.BillingCountry = 'United States';
        }
        Test.startTest();
            update accList;
        Test.stopTest();
        List<Contact> requeryContacts = [SELECT Id, MailingCountry FROM Contact];

        //Assert will pass
        for(Contact a : requeryContacts){
            System.assert(a.MailingCountry != null);
        }
    }
    @IsTest
    static void testERPmethod(){
        List<Account> accList = [SELECT Id, Website, RecordTypeId, BillingCountry FROM Account];
		
        Test.startTest();//Declare Request/Response and URL parameter before calling POST method to test
            summitsAccTriggerHandler.postAccount(accList[0]);
        Test.stopTest();
    }
}