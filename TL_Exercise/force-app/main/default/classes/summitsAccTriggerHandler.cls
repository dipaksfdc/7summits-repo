public class summitsAccTriggerHandler { //Class name's first letter should be Capital Letter SummitsAccTriggerHandler
    // Account Partner Record Type
    public static String partnerRecordType = '01221000001PIifAAG'; // Retrieve RecordTypeID by using Schema method
    //Schema.SobjectType.ObjectName.getRecordTypeInfosByName().get(Label.RecordTypeName).getRecordTypeId();
    
    public static final String username = 'thisIsMyUsername@test.com'; //User protected Custom Setting to store username and password instead of hardcoding
    public static final String password = 'passWord123';
    
    public static void afterUpdate(List<Account> newAccounts) {
        alterContactsCountry(newAccounts);
        RectifyOpportunities(newAccounts);
    }

    public static void beforeUpdate(List<Account> newAccounts) {
        checkWebsite(newAccounts);
    }

    public static void afterInsert(List<Account> newAccounts) {
        sendAccountToERP(newAccounts);
    }
    
    // Every Contact related to an account should have the same mailing country as its parent accounts billing country, 
    // if they do not match, update it, do SOQL in loop
    private static void alterContactsCountry(List<Account> newAccounts){
        List<Contact> contactUpdateList = new List<Contact>();
        for(Account a: newAccounts){
            contactUpdateList.addAll(getMismatchContacts(a, 'MailingCountry', 'BillingCountry'));//Bad practice to querying in loop, optimize query, create map and update and then commit to database
        }
        update contactUpdateList;
    }
    
    // Any account with the record type of partner should have a website, if not add an error for the end user
    public static void checkWebsite(List<Account> accList){
         for(Account a: accList) //Try to add braces for readable code (bad practice if not used)
             if(a.RecordType.Id == partnerRecordType && a.Website == null)
                 a.addError('This Account is a partner account and thus needs a website!');
    }

    public static List<Contact> getMismatchContacts(Account singleAccount, String firstValue, String secondValue){
        String qryString = 'SELECT Id, Name, MailingCountry FROM Contact WHERE ' + firstValue + '!=: +singleAccount.' + secondValue;
        List<Contact> tempContacts = Database.query(qryString);
        for(Contact a: tempContacts){
            a.MailingCountry = singleAccount.BillingCountry;
        }
        return tempContacts;
    }

    // Sum up the $ amount of all opportunities for each account and override the annual revenue field on the account
    public static void RectifyOpportunities(List<Account> newAccounts){
        Map<Id, Decimal> accOpportunitiesMap = new Map<Id, Decimal>();
        List<Account> updateAccRevenue = new List<Account>();
        for(Opportunity a : [SELECT Id, AccountId, Amount FROM Opportunity WHERE AccountId IN: newAccounts]){
            Decimal oppValue = 0;
            if(accOpportunitiesMap.get(a.Account.Id) == null){
                accOpportunitiesMap.put(a.AccountId, a.Amount);
            }
            else{
                oppValue = accOpportunitiesMap.get(a.Account.Id);
                accOpportunitiesMap.put(a.AccountId, a.Amount + oppValue);
            }
        }
        for(Id a : accOpportunitiesMap.keySet()){
            Account acc = new Account(Id = a);//
            acc.AnnualRevenue = accOpportunitiesMap.get(a);
            updateAccRevenue.add(acc);
        }
        try {
            update updateAccRevenue;
        } catch(DmlException e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
        }
    }
    

    // post all account to the external ERP system for billing purposes
    private static void sendAccountToERP(List<Account> accounts){
        for(Account a :accounts){
            HttpResponse res = postAccount(a);
            System.debug('The res ' + res);//Either comment or Remove System Debug statements when checkin to upper environments
        }
    }

    @TestVisible
    private static HttpResponse postAccount(Account acc){
        String payload = JSON.serialize(acc);
        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(headerValue);//Add Basic as a Custom Label or create constant variable in Custom class
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setHeader('Authorization', authorizationHeader); //Add Authorization as a Custom Label or create constant variable in Custom class 
        req.setEndpoint('http://www.randomWebsite.com/'); //Avoid hardcoding URL instead declare in Custom Label. 
        req.setMethod('POST');//Add POST as a Custom Label or create constant variable in Custom class 
        req.setBody(payload);
        HttpResponse res = h.send(req);
        return res;
    }
}