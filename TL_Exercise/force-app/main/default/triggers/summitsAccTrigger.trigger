trigger summitsAccTrigger on Account (before insert, after update, before update) {//after insert has not declare 

    // Only run for North American Countries, we want to exclude international countries for now
    List<Account> northAmericanAcc = new List<Account>();
    getNorthAmericanAccs();
    if (Trigger.isBefore) {
        if (Trigger.isInsert) {
		
        } else if (Trigger.isUpdate) { // instead of else if just else condition will always satisfy 
            summitsAccTriggerHandler.beforeUpdate(northAmericanAcc);
        }
    }
    if(Trigger.isAfter){
        if (Trigger.isInsert) {// Unreachable Code. Logic will not execute because after insert not declared line one 
            summitsAccTriggerHandler.afterInsert(northAmericanAcc);
        } else if (Trigger.isUpdate) {// instead of else if just else condition will always satisfy 
            summitsAccTriggerHandler.afterUpdate(northAmericanAcc);
        }
    }
    
    private static void getNorthAmericanAccs(){ //We could have written this logic in Handler Method instead of writing in trigger
        for(Account a : Trigger.new){
            if(a.BillingCountry != 'United States' || a.BillingCountry != 'Canada' || a.BillingCountry != 'Mexico'  ){
                northAmericanAcc.add(a);
            }
        }
    }
}