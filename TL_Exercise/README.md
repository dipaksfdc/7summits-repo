![](logo.png)
# 7Summits Technical Lead Exercise

This exericse is meant to simulate performing a code review of a pull request that has been submitted by a developer. Please plan on spending 1 - 1.5 hrs on the review.

## Instructions

The developer has created a pull request for your review. Your task is to identify any issues in the code and document them for the developer to fix.

Document the issues you find in the code appropriately, either in a separate document (simulating comments in the pull request) or within the code as comment blocks.

Make sure your review includes an examination of whether the code submitted follows best practices, contains sound logic, and is free of structural and security errors. Be sure to describe both the error in question and the potential fix/workaround.

Create an account and private repository in [BitBucket](https://www.bitbucket.org) to commit your work.

Add Emalee Soddy (​[emalee.soddy@7summitsinc.com](mailto:emalee.soddy@7summitsinc.com)​), Alan Petersen
(​[alan.petersen@7Summitsinc.com](mailto:alan.petersen@7summitsinc.com)​) to the repository as admins when you are ready to have your exercise reviewed.


## Questions to Keep in Mind

**What steps do you typically perform when doing a code review? What things do you look for in the code?**





**What feedback would you give to the developer? What do you expect the developer to do with the feedback?**






## Tools: SFDX and Scratch Orgs
This excercise's metadata is in source format, and can be deployed to a new scratch org. To do so, you will need to have the Salesforce CLI intalled on your system. 

### The Salesforce CLI

The Salesforce CLI (aka SFDX) is an indispensible tool for interacting with Salesforce orgs. The CLI is a requirement for using IDEs like VS Code and IntelliJ, as they simply call the CLI commands from within the UI.

The CLI can be obtained here: [https://developer.salesforce.com/tools/sfdxcli](https://developer.salesforce.com/tools/sfdxcli)

### Registering a Dev Hub
To use a scratch org, you need a Dev Hub from which the scratch org can be created. You can use any production org, or even a developer instance and enable the dev hub in Settings > Dev Hub.

You can then use the `sfdx force:auth:web:login` command to authenticate the dev hub with your SFDX CLI.

### Creating a Scratch Org

A scratch org can be created using the `config/project-scratch-def.json` file. To create a scratch org, use the `force:org:create` SFDX command from within your project:

`sfdx force:org:create -v DevHubName -s -a MyScratchAlias -f config/project-scratch-def.json`

### Deploying Project Code to a Scratch Org
When in your project directory, you can deploy the code in your project to the scratch org using the 

`sfdx force:source:push`

command. 

### Open a Scratch Org
The current project's scratch org can be opened using the 

`sfdx force:org:open -u MyScratchAlias` 

command within the project. This will open your scratch org in your default browser

### Retrieving Project Code from a Scratch Org
Changes made in a scratch org can be retrieved using the following SFDX command:

`sfdx force:source:pull`

SFDX keeps track of changes made by comparing the metadata in the org to that in the project directory. 


